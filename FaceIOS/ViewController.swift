//
//  ViewController.swift
//  FaceIOS
//
//  Created by Dragan Petrovic on 12/05/2018.
//  Copyright © 2018 Dragan Petrovic. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    var camera: Camera?
    var faceIOS: FaceIOSX?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func viewWillAppear(_ annimated: Bool) {
        super.viewWillAppear(annimated)
        faceIOS = FaceIOSX(delegate: self)
        prepareCamera()
        prepareFaceLayer()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    private func prepareFaceLayer() {
        let layer = CAShapeLayer()
        let faceView = UIView(frame: view.bounds)
        faceView.layer.addSublayer(layer)
        layer.frame = view.bounds
        layer.strokeColor = UIColor.red.cgColor
        faceView.transform = CGAffineTransform(scaleX: 1, y: -1)
        view.addSubview(faceView)
    }
    
    private func prepareCamera() {
        camera = Camera(delegate: self)
        Camera.addPreview(on: view)
        Camera.session.startRunning()
    }
}

extension ViewController: CameraDelegate {
    func videoFrameAvailable(frame: CVPixelBuffer) {
        faceIOS?.imageAvailable(cvPixelBuffer: frame)
    }
}

extension ViewController: FaceIOSXDelegate {
    func bounds(_ frame: CGRect) {
        view.subviews.last?.layer.sublayers?.removeAll()
    }
    
    func points(_ points: [CGPoint]?, boundingBox: CGRect) {
        guard let layer = Face.draw(points: points, bounds: boundingBox * view.bounds) else { return }
        view.subviews.last?.layer.addSublayer( layer )
    }
}
