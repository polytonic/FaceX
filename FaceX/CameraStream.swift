//
//  CameraStream.swift
//  FaceX
//
//  Created by Dragan Petrovic on 06/05/2018.
//  Copyright © 2018 Dragan Petrovic. All rights reserved.
//

#if os(OSX)
import Cocoa
#elseif os(iOS)
import UIKit
#endif
import AVFoundation

protocol CameraDelegate: class {
    func videoFrameAvailable(frame: CVPixelBuffer)
}

class Camera: NSObject {
    static let session = AVCaptureSession()
    weak var delegate: CameraDelegate?
    init?(delegate: CameraDelegate, quality: AVCaptureSession.Preset = .low) {
        guard let device = AVCaptureDevice.default(for: .video) else { return nil }
        super.init()
        var input: AVCaptureDeviceInput
        do { try input = AVCaptureDeviceInput(device: device) } catch { return nil }
        let output = AVCaptureVideoDataOutput()
        output.alwaysDiscardsLateVideoFrames = true
        #if os(OSX)
        output.videoSettings = [
            kCVPixelBufferPixelFormatTypeKey as String: kCVPixelFormatType_32BGRA,
            kCVPixelBufferCGBitmapContextCompatibilityKey as String: true
        ]
        #elseif os(iOS)
        output.videoSettings = [
            kCVPixelBufferPixelFormatTypeKey as String: kCVPixelFormatType_32BGRA
        ]
        #endif
        output.setSampleBufferDelegate(self, queue: DispatchQueue(label: "facex.camera"))
        Camera.session.beginConfiguration()
        Camera.session.sessionPreset = quality
        Camera.session.addInput(input)
        Camera.session.addOutput(output)
        Camera.session.commitConfiguration()
        self.delegate = delegate
    }
    
    static func addPreview(on view: Any) {
        let preview = AVCaptureVideoPreviewLayer(session: Camera.session)
        preview.videoGravity = .resizeAspectFill
        preview.connection?.videoOrientation = .portrait
        #if os(iOS)
        let superView = view as? UIView
        superView?.layer.addSublayer(preview)
        #elseif os(OSX)
        let superView = view as? NSView
        superView?.layer = preview
        #endif
        preview.frame = superView?.bounds ?? CGRect.zero
    }
}

extension Camera: AVCaptureVideoDataOutputSampleBufferDelegate {
    func captureOutput(_ captureOutput: AVCaptureOutput,
                       didOutput sampleBuffer: CMSampleBuffer,
                       from connection: AVCaptureConnection) {
        guard let pixelBuffer = CMSampleBufferGetImageBuffer(sampleBuffer) else { return }
        CVPixelBufferLockBaseAddress(pixelBuffer, .readOnly)
        delegate?.videoFrameAvailable(frame: pixelBuffer)
        CVPixelBufferUnlockBaseAddress(pixelBuffer, .readOnly)
    }
}

