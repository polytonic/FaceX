//
//  Helpers.swift
//  FaceX
//
//  Created by Dragan Petrovic on 11/05/2018.
//  Copyright © 2018 Dragan Petrovic. All rights reserved.
//

#if os(OSX)
import Cocoa
#elseif os(iOS)
import UIKit
#endif
import AVKit

// MARK: - @public
extension CGRect {
    static func *(other: CGRect, this: CGRect) -> CGRect {
        return CGRect(x: other.minX * this.width,
                      y: other.minY * this.height,
                      width: other.width * this.width,
                      height: other.height * this.height)
    }
}

extension CGPoint {
    static func *(frame: CGRect, this: CGPoint) -> CGPoint {
        return CGPoint(x: frame.minX + this.x * frame.width,
                       y: frame.minY + this.y * frame.height)
    }
}

struct Face {
    static func draw(points: [CGPoint]?, bounds: CGRect) -> CALayer? {
        guard let points = points else { return nil }
        let shapeLayer = CAShapeLayer()
        #if os(OSX)
        shapeLayer.strokeColor = NSColor.white.cgColor
        shapeLayer.fillColor = NSColor.black.cgColor
        let path = NSBezierPath()
        #elseif os(iOS)
        shapeLayer.strokeColor = UIColor.white.cgColor
        shapeLayer.fillColor = UIColor.black.cgColor
        let path = UIBezierPath()
        #endif
        shapeLayer.lineWidth = 2.0
        path.move(to: bounds * points[0])
        for point in points {
            #if os(OSX)
            path.line(to: bounds * point)
            #elseif os(iOS)
            path.addLine(to: bounds * point)
            #endif
        }
            
        #if os(OSX)
        path.line(to: bounds * points[0])
        #elseif os(iOS)
        path.addLine(to: bounds * points[0])
        #endif
        shapeLayer.path = path.cgPath
        return shapeLayer
    }
}

// MARK: - @fileprivate
#if os(OSX)
fileprivate extension NSBezierPath {
    var cgPath: CGPath {
        let path = CGMutablePath()
        var points = [CGPoint](repeating: .zero, count: 3)
        for idx in 0..<elementCount {
            switch element(at: idx, associatedPoints: &points) {
                case .moveToBezierPathElement: path.move(to: CGPoint(x: points[0].x, y: points[0].y))
                case .lineToBezierPathElement: path.addLine(to: CGPoint(x: points[0].x, y: points[0].y))
                case .closePathBezierPathElement: path.closeSubpath()
                case .curveToBezierPathElement: break
            }
        }
        return path
    }
}
#endif
