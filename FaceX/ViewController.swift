//
//  ViewController.swift
//  FaceX
//
//  Created by Dragan Petrovic on 12/05/2018.
//  Copyright © 2018 Dragan Petrovic. All rights reserved.
//

import Cocoa
import AVFoundation

class ViewController: NSViewController {
    var camera: Camera?
    var faceX: FaceIOSX?
    
    override func viewWillAppear() {
        super.viewWillAppear()
        view.wantsLayer = true
        faceX = FaceIOSX(delegate: self)
        camera = Camera(delegate: self)
        Camera.addPreview(on: view)
        Camera.session.startRunning()
        view.layer?.addSublayer(CAShapeLayer())
    }
}

extension ViewController: CameraDelegate {
    func videoFrameAvailable(frame: CVPixelBuffer) {
        faceX?.imageAvailable(cvPixelBuffer: frame)
    }
}

extension ViewController: FaceIOSXDelegate {
    func bounds(_ frame: CGRect) {
        view.layer?.sublayers?.last?.sublayers?.removeAll()
    }
    
    func points(_ points: [CGPoint]?, boundingBox: CGRect) {
        guard let layer = Face.draw(points: points,
                                    bounds: boundingBox * view.bounds) else { return }
        view.layer?.sublayers?.last?.addSublayer(layer)
    }
}
