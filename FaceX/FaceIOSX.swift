//
//  FaceIOSX.swift
//  FaceIOSX
//
//  Created by Dragan Petrovic on 11/05/2018.
//  Copyright © 2018 Dragan Petrovic. All rights reserved.
//

import Vision

enum RequestType: Int { case face = 0, landmarks }

// MARK: - @public(protocol)
protocol FaceIOSXDelegate: class {
    func bounds(_ frame: CGRect)
    func points(_ points: [CGPoint]?, boundingBox: CGRect)
}

class FaceIOSX {
// MARK: - @private(var)
    private weak var delegate: FaceIOSXDelegate?
    private lazy var faceRequest: VNDetectFaceRectanglesRequest = {
        return VNDetectFaceRectanglesRequest()
    }()
    
    private lazy var landmarksRequest: VNDetectFaceLandmarksRequest = {
        return VNDetectFaceLandmarksRequest()
    }()
    
// MARK: - @public
    init(delegate: FaceIOSXDelegate) {
        self.delegate = delegate
    }

    func imageAvailable(cvPixelBuffer: CVPixelBuffer) {
        observe(handler: VNImageRequestHandler(cvPixelBuffer: cvPixelBuffer, options: [:]))
    }

// MARK: - @private
    private typealias ObservationResults = [VNFaceObservation]
    private func observe(handler: VNImageRequestHandler) {
        do { try handler.perform([faceRequest, landmarksRequest]) } catch {
            print("faceRectangleHandler \(error)")
            return
        }
        guard let face = faceRequest.results as? ObservationResults,
            let landmarks = landmarksRequest.results as? ObservationResults else { return }
        #if os(iOS)
        DispatchQueue.main.async { self.delegate?.bounds(CGRect.zero) }
        #endif
        for (index, results) in [face, landmarks].enumerated() where results.count > 0  {
            switch RequestType(rawValue: index)! {
            case RequestType.face:
                landmarksRequest.inputFaceObservations = results
                DispatchQueue.main.async { self.delegate?.bounds(results.first?.boundingBox ?? CGRect.zero) }
            case RequestType.landmarks:
                for result in results {
                    DispatchQueue.main.async {
                        if let landmarks = result.landmarks {
                            /* Draw Regions */
                            self.drawRegions(for: landmarks)
                            /* Or all points
                            self.drawAllPoints(for: landmarks) */
                        }
                    }
                }
            }
        }
    }

// MARK: - @draw(private)
    private func drawRegions(for landmarks: VNFaceLandmarks2D) {
        let r = landmarksRequest.inputFaceObservations?.first?.boundingBox ?? CGRect.zero
        delegate?.points(landmarks.faceContour?.normalizedPoints, boundingBox: r)
        delegate?.points(landmarks.leftEye?.normalizedPoints, boundingBox: r)
        delegate?.points(landmarks.rightEye?.normalizedPoints, boundingBox: r)
        delegate?.points(landmarks.nose?.normalizedPoints, boundingBox: r)
        delegate?.points(landmarks.noseCrest?.normalizedPoints, boundingBox: r)
        delegate?.points(landmarks.outerLips?.normalizedPoints, boundingBox: r)
        delegate?.points(landmarks.innerLips?.normalizedPoints, boundingBox: r)
        delegate?.points(landmarks.leftEyebrow?.normalizedPoints, boundingBox: r)
        delegate?.points(landmarks.rightEyebrow?.normalizedPoints, boundingBox: r)
        delegate?.points(landmarks.leftPupil?.normalizedPoints, boundingBox: r)
        delegate?.points(landmarks.rightPupil?.normalizedPoints, boundingBox: r)
    }
    
    private func drawAllPoints(for landmarks: VNFaceLandmarks2D) {
        let r = landmarksRequest.inputFaceObservations?.first?.boundingBox ?? CGRect.zero
        delegate?.points(landmarks.allPoints?.normalizedPoints, boundingBox: r)
    }
}

