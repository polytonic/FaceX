//
//  AppDelegate.swift
//  FaceX
//
//  Created by Dragan Petrovic on 12/05/2018.
//  Copyright © 2018 Dragan Petrovic. All rights reserved.
//

import Cocoa

@NSApplicationMain
class AppDelegate: NSObject, NSApplicationDelegate {



    func applicationDidFinishLaunching(_ aNotification: Notification) {
        // Insert code here to initialize your application
    }

    func applicationWillTerminate(_ aNotification: Notification) {
        // Insert code here to tear down your application
    }


}

